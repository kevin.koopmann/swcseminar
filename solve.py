from pyomo.environ import SolverFactory, value
from pyomo.opt import SolverStatus
from budgetflow import model
from time import time
import pygraphviz as pgv
import argparse
from generator import generate

def external(args):
    if(args.generate):
        # Generate sample case
        edgeCount = generate(args.input, *args.generate)
        print('Generated random architecture with {} edges.'.format(edgeCount))

    instance = model.create_instance(args.input)
    opt = SolverFactory('gurobi')
    results = opt.solve(instance, tee=True)
    instance.solutions.store_to(results)
    output_path = 'output/%s' % args.output
    results.write(filename=output_path)

    # Display graph
    if(results.solver.status != SolverStatus.ok):
        print('Problem infeasible')
        quit()
            
    if(not args.skip):
        G = pgv.AGraph(directed=True)
        G.add_nodes_from(instance.N)
        for (i,j) in instance.A:
            G.add_edge(i,j)
            flow = value(instance.f[i,j])
            edge = G.get_edge(i,j)
            edge.attr['label'] = flow
            if(flow <= 0):
                edge.attr['color'] = 'grey'
        for k in instance.LL:
            node = G.get_node(k)
            if(instance.app[k]):
                node.attr['shape'] = 'box'
                node.attr['color'] = 'red'
            else:
                node.attr['color'] = 'grey'
        G.layout(prog='dot')
        G.draw('output/graph.png')

def autotest(args):
    f = open(args.output, 'w')
    f.write('RUN,Budgets,Upper,Caps,Lower,TransMin,TransMax,BudgMin,BudgMax,AppMin,AppMax,DensBudg,DensUpp,DensLow,Variables,Edges,Time\n')

    for run in range(0, args.runs):
        variables = []
        runtimes = []
        edgeCounts = []

        for avgCount in range(0, args.average):
            edgeCount = generate('.temp.dat', *args.genparams)
            edgeCounts.append(edgeCount)
            print('Generated random architecture in run {} at average iteration {} with {} edges.'.format(run, avgCount, edgeCount))
        
            starttime = time()
            instance = model.create_instance('.temp.dat')
            opt = SolverFactory('gurobi')
            results = opt.solve(instance)
            instance.solutions.store_to(results)
            endtime = time()
            variables.append(int(results["Problem"][0]["Number of variables"]))
            runtimes.append(endtime - starttime)

        # Write data
        f.write('{},'.format(run))
        for arg in args.genparams:
            f.write('{},'.format(arg))
        f.write('{},{},{}\n'.format(sum(variables)/len(variables), sum(edgeCounts)/len(edgeCounts), sum(runtimes)/len(runtimes)))

        if(args.increment):
            for incrSet in args.increment:
                target = int(incrSet[0])
                args.genparams[target] += incrSet[1]
                print('Now at {} for parameter {}.'.format(args.genparams[target], target))

        if(args.decrement):
            for decrSet in args.decrement:
                target = int(decrSet[0])
                args.genparams[target] -= decrSet[1]
                print('Now at {} for parameter {}.'.format(args.genparams[target], target))


parser = argparse.ArgumentParser(description='EA Integer Programming Solver.')
subparsers = parser.add_subparsers(help='sub-command help')
parser_autotest = subparsers.add_parser('autotest', help='performs an automated series of testruns')
parser_run = subparsers.add_parser('run', help='runs the enterprise architecture optimization once')

parser_autotest.add_argument('runs', metavar='RUNS', type=int, help='the number of testruns')
parser_autotest.set_defaults(func=autotest)
parser_autotest.add_argument('genparams', type=float, nargs=13, metavar=('NUMBER_BUDGETS', 'NUMBER_UPPER', 'NUMBER_CAP', 'NUMBER_LOWER', 'TRANSITIONSCOSTS_MIN', 'TRANSITIONSCOSTS_MAX', 'BUDGET_MIN', 'BUDGET_MAX', 'APPLICATIONCOSTS_MIN', 'APPLICATIONCOSTS_MAX', 'DENSITY_BUDGET', 'DENSITY_UPPER', 'DENSITY_LOWER'),
        help='parameters for generation')
parser_autotest.add_argument('output', type=str, help='the name of the output file')
parser_autotest.add_argument('--increment', type=float, nargs=2, action='append', metavar=('TARGET', 'STEP'), help='increments target parameter by step in each run')
parser_autotest.add_argument('--decrement', type=float, nargs=2, action='append', metavar=('TARGET', 'STEP'), help='decrements target parameter by step in each run')
parser_autotest.add_argument('--average', type=int, default=1, help='averages runtime values over specified iterations in each run')

parser_run.add_argument('input', metavar='INPUT', type=str, help='the path to the input file')
parser_run.add_argument('-o', '--output', dest='output', type=str, default='results.yml', help='the name of the output file (to be stored in ./output)')
parser_run.add_argument('-g', '--generate', dest='generate', type=float, nargs=13, 
        metavar=('NUMBER_BUDGETS', 'NUMBER_UPPER', 'NUMBER_CAP', 'NUMBER_LOWER', 'TRANSITIONSCOSTS_MIN', 'TRANSITIONSCOSTS_MAX', 'BUDGET_MIN', 'BUDGET_MAX', 'APPLICATIONCOSTS_MIN', 'APPLICATIONCOSTS_MAX', 'DENSITY_BUDGET', 'DENSITY_UPPER', 'DENSITY_LOWER'),
        help='generate sample architecture (overwrites INPUT)')
parser_run.add_argument('--skip-plot', dest='skip', action='store_true', help='skip the generation of a visual graph output')
parser_run.set_defaults(func=external)
args = parser.parse_args()
args.func(args)
