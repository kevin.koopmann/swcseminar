Building on our implementation we will now provide both a reasoning for our expectations pertaining to the algorithm's runtime, as well as an experimental evaluation of this runtime. This will allow us to predict the practical value of the solution we have presented.  

\subsection{Scalability}
\label{ss:scalability}
\subsubsection{Runtime Expectations}
As is evident from eq. \ref{ip:obj} the objective function of our model is a linear expression and dependent upon the flow on edges between applications and the sink node $t$. The number of linear objective terms in the model is therefore equivalent to the number of applications. 

We expect an increase in the number of edges, for instance, due to a positive variation of the amount of budget, cost center and capability nodes, to lead to linear increases in runtime due to the additional linear constraint terms. 

\subsubsection{Experimental Runtime Evaluation}
For experimental evaluation, we have generated random architecture representations to be processed by our software with different values for the number of budgets, cost centers, capabilities and applications. For the first part of the evaluation, all density values were set to $0.1$, and for each of these value sets the average runtime and variable count over 20 runs was determined on a system with 102 GFlops\footnote{determined using LINPACK} of floating-point performance and 8 GB of RAM. At this density we expect the variable count to be closely related to the number of edges.

Table \ref{table:varying} shows the resulting runtimes for varying numbers of budgets, cost centers, capabilities and applications with fixed ratios at a connectivity density of $0.1$. We have decided on these values, as they represent a common balance between the various entities, even though the connection density is higher than in most real-world architectures. \cite{lagerstrom2010architecture} However, this reduces the impact of time spent in pre-solving and optimization phases on overall measurement results, and is still representative of actual use cases, as we will show the relationship between density and runtime to be inversely proportional. 

Time measured includes time spent on problem generation by Pyomo and time in the Gurobi solver, including preprocessing and pre-solving time. Figure \ref{fig:eval_regression} shows the runtime of the solver compared to the number of variables corresponding to edge and vertex counts. We have fitted a linear model to our measurements using MATLAB and present the result in Figure \ref{fig:eval_regression} as well.

\begin{table*}
	\caption{Runtime in Solver for varying numbers of budgets, cost centers, capabilities and applications with fixed ratios, average over 20 runs}
	\centering
	\begin{tabular}{l|l|l|l || l|l|l || l}%
	\bfseries Budgets & \bfseries Cost Centers & \bfseries Capabilities & \bfseries Applications & \bfseries Density & \bfseries Variables & \bfseries Edges & \bfseries Runtime
	\csvreader[head to column names]{figures/test_variables.csv}{}
	{\\\Budgets & \Upper & \Caps & \Lower & \DensBudg & \Variables & \Edges & \Time s}
\end{tabular}
	\label{table:varying}
\end{table*}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\textwidth]{figures/eval_regression.pdf}
	\caption{Time spent in solver and linearly fitted corresponding model (data from Table \ref*{table:varying})}
	\label{fig:eval_regression}
\end{figure}

This shows that with increasing architectural complexity measured in terms of connectivity between different entities in the organization graph, in accordance with our expectations, the runtime of our algorithm increases linearly. We have found that this relationship holds in most cases for architectures with equivalent connection densities.

By varying connection densities in a second test run we confirm that execution time is not solely dependent on the edge count, but on connectivity as well. The results of these measurements are shown in Table \ref{table:densities}. 

Comparison of varying density values against the average time spent in the solver for each edge in the architecture yields Figure \ref{fig:eval_densities}. This is to show that connection density is inversely linearly related to execution time.

\begin{table*}
	\caption{Runtime in Solver for with varying node counts and varying connectivity, average over 10 runs}
	\centering
	\begin{tabular}{l|l|l|l || l|l|l || l}%
		\bfseries Budgets & \bfseries Cost Centers & \bfseries Capabilities & \bfseries Applications & \bfseries Density & \bfseries Variables & \bfseries Edges & \bfseries Runtime
		\csvreader[head to column names]{figures/test_density.csv}{}
		{\\\Budgets & \Upper & \Caps & \Lower & \DensBudg & \Variables & \Edges & \Time s}
	\end{tabular}
	\label{table:densities}
\end{table*}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\textwidth]{figures/eval_densities.pdf}
	\caption{Seconds per edge depending on graph connectivity (data from Table \ref*{table:densities})}
	\label{fig:eval_densities}
\end{figure}

We assume that the reduction in runtime in very highly connected architectures compared to architectures with lower edge counts is related to particular details regarding the implementation of the relaxation or SOCP algorithms of the solver we have used, or in the simplification of certain problem parameters in highly connected graphs. However, a detailed analysis of this effect is beyond the scope of this article.

In the following section we will analyze the consequences this entails for the application of this algorithm on real-world optimization problems. 

\subsection{Value for Real-World Applications}
As is evident from our runtime analysis, the execution time of the Integer Program we have described is dependent upon a multitude of factors, including the number of entities on different architectural layers and the connectivity between such entities. There was a tendency towards a linear correlation between variable or edge count and runtime for all evaluated architectures. 

Even at linear runtimes, this may still present challenges for scalability towards optimizing extremely large-scale enterprise architectures. Even though such optimizations will not be performed frequently in most cases, and can often be executed on large-scale systems, it is conceivable that on certain architectures execution time might prove to be prohibitive. 

However, most architectures encountered in real-world applications seem to be of a rather limited size in comparison to the scenarios of our evaluation model that required high execution times. Lagerstrom et al.\cite{lagerstrom2010architecture} for instance considered an architecture consisting of 407 nodes and 1157 edges, translating to a density of just $0.00698$ in our notation. Evaluating a similar architecture on our test system consistently yielded a runtime of less than one second. 