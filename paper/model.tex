Before presenting our approach we first give a quick overview in subsection \ref{ss:TowardsOM} of how EAs are modeled in literature. Later on, we especially take care of the foundations which are already acquired by Hacks and Lichter. Based on these foundations we provide our optimizing approach.

\subsection{Towards an Optimization Model}
\label{ss:TowardsOM}
According to Winter and Fisher \cite{winter2006essential} enterprise architectures are most commonly composed of hierarchical layers. Each of these layers consists of various architectural artifacts which can be connected through different relations with elements of the same layer. Additionally, artifacts on a layer can be explicitly influenced by elements on the superseding layer, reflecting a priority of importance of decisions undertaken on higher levels of the architecture. \cite{mesarovic2000theory}

For instance, an enterprise architecture could consist of business entities such as "Human Resources", "Enterprise Resource Planning" and "Liquidity Management", each requiring a certain set of capabilities. Such capabilities could be functions such as "Hiring" or "Stock Trading". This is modeled by directed edges from the business entities to the capability nodes. Furthermore, applications selectively implement certain capabilities, represented through edges from the capabilities to implementing applications. Figure \ref{fig:concrete_model} shows such an exemplary architecture where the layers are named as \textit{Cost Centers} and \textit{Applications}. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.30\textwidth]{figures/graph_model_concrete.pdf}
	\caption{Concrete example for two layers in an enterprise architecture (reproduced from Hacks and Lichter \cite{hacks2018optimierung})}
	\label{fig:concrete_model}
\end{figure}

An example of an application of this concept could be the simplification of an IT business infrastructure in order to reduce costs. A business resource may require certain capabilities (most times functionalities) fulfilled by applications on the application layer. An optimization model, in this case, would be suitable to reduce the operational cost of the architecture by selecting a set of applications that implements all required capabilities at the smallest possible cost to the enterprise. 

In order to be able to apply common graph algorithms to the solution of problems related to the enterprise architecture, we will model these architectures as directed graphs, as suggested by Hacks and Lichter \cite{hacks2018optimierung}. An element of the architecture describing an aggregation hierarchy will be represented by vertices, and the edges between these vertices mirror dependencies between elements on different layers of the architecture.

In our subsequent analysis of the optimization problem for enterprise architectures we will consider only the interactions between two directly adjacent layers of an overall architecture. This is permissible because decisions on higher layers of the architecture reduce the degree of freedom on subsequent layers \cite{mesarovic2000theory}, and greatly simplify understanding of the optimization process. To perform an analysis of the entire architecture, simply repeat the analysis in top-down for each interface between two layers.

Hacks and Lichter \cite{hacks2017optimizing} present different objectives that could be used to optimize the model, such as minimal coupling, amount of lower layer elements or operational costs. In real-world applications, we often find certain business entities, such as departments, to be restricted to a specific budget. These often yearly scheduled budgets are important for business managers to estimate the project and department costs. It is common that various departments are sharing the same IT infrastructure so that each department has its own scot, which needs a specific budget. We will extend the model by considering these budget restrictions, while also incorporating transitional costs for the introduction or decommissioning of currently active applications in the architecture. This seems to be warranted as moving away from old applications or implementing new ones often arises substantial costs necessary for example to educate users, transfer data or resolving other dependencies. 

\subsection{Foundations}
Building on this initial understanding of enterprise architecture modeling and objectives in its optimization we will now introduce a more formal representation of these architectures.

As described by Hacks and Lichter \cite{hacks2017optimizing} we want to represent the enterprise architecture as a quadruple $EA = \left( \mathcal{L}, \mathcal{C}, E, R \right)$ where $\mathcal{L}$ is an ordered set of architectural layers, $\mathcal{C}$ describes a set of sets of capabilities, $E$ is a set of architectural elements, and $R$ represents a set of relations between these elements and capabilities.

We assume each layer $L \in \mathcal{L}$ to consist of architectural elements so that $L \subset E$, and to be disjunct as prescribed by eq. \ref{eq:disjunct_layers}.

\begin{align}
	L_i \cap L_j = \emptyset && \forall L_i, L_j \in \mathcal{L} \quad i \neq j \label{eq:disjunct_layers}
\end{align}

For each layer element $ul_i \in L_j$ of the upper layer there may be an associated budget $b_i \in \mathbb{N}_0$. For each element on the lower layer $ll_i \in L_{j+1}$ there is an associated operational cost $p_i$ and a transitional cost $tp_i$. 

Furthermore we place capabilities on interfaces between two adjacent layers. For each such interface between $L_i$ and $L_{i+1}$ we assign the symbol $\mathcal{C}_{i}$. As each capability is associated with a specific business requirement we assume all capabilities to be unique to an interface between two adjacent architectural layers.

\begin{align}
	\mathcal{C}_i \cap \mathcal{C}_j = \emptyset && \forall \mathcal{C}_i, \mathcal{C}_j \in \mathcal{C} \quad i \neq j
\end{align}

The set of relations consists of tuples of architectural elements and capabilities, which couples upper layer elements with capabilities and capabilities with lower layer elements.

\begin{align}
	R \subset \{ (e, c): e \in L_i, c \in \mathcal{C}_i \} \cup \{ (c, e): c \in \mathcal{C}_i, e \in L_{i+1} \}
\end{align}

This object $EA$ is the subject of optimization.

\subsection{Modeling Cash Flows in Enterprise Architectures}
Hacks and Lichter \cite{hacks2017optimizing} present a solution to the optimization problem for enterprise architectures by introducing intermediate relations between all elements of an adjacent layer constituting a bipartite graph. In order to be able to incorporate budget constraints into our model in a meaningful manner and benefit from graph algorithms we chose a different approach by modeling cost flows originating from budget nodes. 

In order to obtain the desired solution we apply a modified maximum flow problem to a graph we construct from input parameters of the problem. The maximum flow is concerned with obtaining a maximum flow through a single-source single-sink flow network by assigning a feasible flow $f(e) \in \mathbb{R}_+$ to all edges $e \in \mathcal{E}$, such that the total flow $\sum F_{i,t}, (i,t) \in \mathcal{E}$ into the sink is maximal. Additionally, the maximum flow problem imposes an additional constraint by limiting the flow across an edge to a certain capacity limit $c$. Naturally, in a flow network, the flow out of a node must be equivalent to the flow entering the node, excepting source and sink nodes. Instead of considering a maximum flow across the network, we want to minimize cost flow, and we limit the flow across certain budget-related arcs to a specific budget value. In linear programming terms, we formulate this objective in eq. \ref{ip:obj}, and formulate these initial constraints in \ref{ip:budgets}, and \ref{ip:flow}. \cite{trevisan2010cs261}

We begin to construct a graph with vertices $\mathcal{V}$ consisting of elements from two adjacent layers $L_i$ and $L_{i+1}$, as well as capabilities $\mathcal{C}_i$. We also start with a set of edges $\mathcal{E}$ equivalent to the set of relations $R$ for these adjacent layers. 

We also add a start node $s$ as an entry point for a flow algorithm. Furthermore, for each budget $b_i$ it is necessary to construct a node and add relations to all upper layer elements associated with this budget. For additional relations, we introduce for each budget between $s$ and the budget node we then apply constraints to these relations to ensure a maximum cost flow equivalent to the budget.

From each of the lower layer elements, we construct additional relations to the sink node $t$. The activation of a lower layer element bears operational costs and may additionally arise transitional costs. The deactivation of a lower layer application may also arise transitional costs, but no operational costs. To incorporate this requirement into our cash flow model, we must also enforce an additional constraint to force the flow on these edges to be equivalent to exactly the cost arised by (de-)activation of the element. 

This model represents the flow of monetary resources originating from operational budgets through the architecture. To optimize towards minimal costs, we can now simply attempt to minimize the flow of cash through the network.

\subsection{Final Modelling}
\label{ss:final_modelling}
To summarize our findings we will now combine the previous results to formulate the following selection problem for two adjacent layers of the architecture. 

\begin{enumerate}
	\item All upper layer elements $ul_i$ are to be implemented. We also refer to these elements as \emph{cost centers}. 
	\item The implementation of a distinct upper layer element $ul_i$ requires fulfilling a known subset $c_i \subset C$ with $C = \biguplus_{\mathcal{C}_{i} \in \mathcal{C}}{\mathcal{C}_{i}}$ of capabilities. We refer to those subsets indicating the necessary capabilities for the operation of a cost center as the cost center's \emph{capability set}.
	\item Fulfilling a capability $C_i$ requires the activation of at least one element of a known subset $S_i$ of lower layer elements $ll_i$. We refer to these lower layer elements as \emph{applications}. Furthermore, we refer to the subset $S_i$ which indicates the applications suitable for implementing a capability as the capability's \emph{implementing set}. These implementing sets are not mutually exclusive. If an application belongs to multiple implementing sets, then selecting the application for the solution would simultaneously satisfy all implementing sets it is contained in, and thereby all corresponding capabilities.
	\item An application which has been activated arises an operational cost $p_i$.
	\item Implementing an application which has previously been inactive arises an additional transitioning cost $tp_i$, and vice versa.
	\item All cost centers are subject to budget constraints. These budgets are distributed among multiple cost centers and are not mutually exclusive. That is, a cost center can receive funds from multiple designated budgets, and a budget can be designated for multiple upper layer elements.
\end{enumerate}

The problem we consider is to determine which applications should be implemented in order to minimize operational and transitional costs, as well as how the budgets are consumed by the cost centers in order to realize all capabilities. This combines all the requirements that we have previously formulated.

In order to accomplish this, we derive a network flow minimization problem from the selection problem and formulate the following integer program. We first construct a graph from our problem description. Let $\mathcal{V}$ denote the set of vertices, and $\mathcal{E}$ the set of edges.

Let $s$ be a source, and $t$ be a sink node, and let $\mathcal{V}_{-}$ be the set of vertices $\mathcal{V}$ excluding these nodes $s$ and $t$. Construct a node $B_i$ for each budget $b_i$ and edges $(B_i, ul_j)$ for each cost center $ul_j$ assigned to this budget. Construct one edge $(s, B_i)$ for each budget node $B_i$. 

Construct a capability node $C_i$ for each capability $\mathcal{C}_i$, and let $(ul_j, C_i)$ be the edges from each cost center $ul_j$ to the capabilities it requires. Then let $(C_i, ll_j)$ be the edges from the capability $C_i$ to all applications $ll_j$ it is implemented by. 

Finally, construct one edge $(ll_i, t)$ for each application $ll_i$. 

Figure \ref{fig:abstract_model} shows an example architecture modelled accordingly.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.35\textwidth]{figures/graph_model_abstract.pdf}
	\caption{Abstract example for two layers in an enterprise architecture}
	\label{fig:abstract_model}
\end{figure}

Let $\mathcal{Z}$ be the set of applications already implemented (as to be considered for transitional analysis), and $z_i \in \mathcal{Z}$ be the implementation state of the application $A_i$. $F_{i,j}$ designates the flow between two nodes $(i, j)$ and $A_i$ is a boolean determining whether application $ll_i$ is to be implemented. Let $tn_i$ be a boolean variable indicating whether a transition is necessary for application $ll_i$. These are the variables of the integer program.

The integer program describing the problem we have specified can now be defined as follows.

\begin{align}
	\text{Minimize} &&& \sum_{\substack{(i,t) \in \mathcal{E}}} F_{i,t} \label{ip:obj} \\
	\text{subject to} && F_{i,j} &\leq b_{j} \quad \forall (i,j) \in \mathcal{E}, j \in \mathcal{B} \label{ip:budgets} \\
	\text{and} && \sum_{\substack{(i,j) \in \mathcal{E}}} A_j &\geq 1 \quad \forall i \in \mathcal{C} \label{ip:capabilities} \\
	\text{and} && tn_i &\geq A_i - z_i \quad \forall (i,t) \in \mathcal{E} \label{ip:tn1} \\
	\text{and} && tn_i &\geq z_i - A_i \quad \forall (i,t) \in \mathcal{E} \label{ip:tn2} \\
	\text{and} && F_{i,t} &= \left( A_i \cdot p_i \right) +  \left( tp_i \cdot tn_i \right)  \label{ip:apps} \\ 
	&&& \forall (i,t) \in \mathcal{E}  \notag \\
	\text{and} && \sum_{\substack{(i,k) \in \mathcal{E}}} F_{i,j} &= \sum_{\substack{(k,j) \in \mathcal{E}}} F_{i,j} \quad \forall k \in \mathcal{V}_{-} \label{ip:flow}
\end{align}

~\ref{ip:obj} describes the objective function. As we are attempting to minimize accruing costs, our objective is the minimization of flow across all incoming arcs at the sink node $t$.

~\ref{ip:budgets} enforces budget limits on the edges connecting the source $s$ to the budget nodes.

~\ref{ip:capabilities} ensures that at least one of the applications connected to a capability is activated.

~\ref{ip:tn1} and~\ref{ip:tn2} set the variable $tn_i$ that indicates whether a transition of application $A_i$ is necessary to the correct value. 

~\ref{ip:apps} enforces a valid flow on the edges connecting the applications to the sink $t$; that is, the flow is either $0$ if the application is disabled, or exactly the sum of operational and transitional costs. This is accomplished by adding up the product of the boolean variable $A_i$ defining whether the application is to be activated and the corresponding cost $p_i$, and the transitional cost function.

~\ref{ip:flow} is a constraint common to all flow problems and forces the amount of flow entering a node to be equal to the amount of flow leaving it.

Thereby we have implemented all requirements originally specified in the definition of the selection problem for budgeted enterprise architecture optimization.

The variables $A_i$ will contain the implementation state for application $ll_i$ in the optimal solution, and $F_{i,j}$ will describe the amount of cash flow between the two entities belonging to $i$ and $j$. 

\subsection{Applying the Optimization Model}
To apply the optimization model to an example graph the budget constraints have to be implemented as nodes between the cost centers and the start node as described in subsection \ref{ss:final_modelling}. The edges between the start and the budget nodes function as the budget constraints according to (\ref{ip:budgets}) in section \ref{ss:final_modelling}. Cost-centers are provided by different budgets which are represented as the connecting edges between those nodes. Figure \ref{fig:example_model} shows an example of such a graph where the cost centers \textit{Human Resources} and \textit{Liquidity Management} are each sharing their budgets with \textit{ERP}. 
\begin{figure}
	\centering
	\includegraphics[width=0.40\textwidth]{figures/graph_model_example.pdf}
	\caption{Concrete example with budgets and costs}
	\label{fig:example_model}
\end{figure}
Here \textit{Application 1} and \textit{Application 2} are not yet used in the model and can be activated by applying the denoted transition costs. To calculate an optimal solution we apply our above-described approach to such a graph with the given costs. Figure \ref{fig:example_model_solution} sketches the optimal solution calculated by our model regarding a minimum cost-flow. This optimal solution suggests to include \textit{Application 1} and \textit{Application 2} with a total cost of 70 for each consisting of transition and operating costs. The total costs for this solution amounts to 165 and is reflected to the minimum cost-flow in the network.


\begin{figure}
	\centering
	\includegraphics[width=0.40\textwidth]{figures/graph_model_example_solution.pdf}
	\caption{Solution for concrete example with cash flow}
	\label{fig:example_model_solution}
\end{figure}

