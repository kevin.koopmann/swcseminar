import sys
from random import *


#print function without automatic return
def writeline(file, val):
    file.write(val + '\n')

def generate(file, NUMBER_BUDGETS, NUMBER_UPPER, NUMBER_CAP, NUMBER_LOWER, TRANSITIONSCOSTS_MIN, TRANSITIONSCOSTS_MAX, BUDGET_MIN, BUDGET_MAX, APPLICATIONCOSTS_MIN, APPLICATIONCOSTS_MAX, DENSITY_BUDGET, DENSITY_UPPER, DENSITY_LOWER):
    f = open(file, 'w')

    upperLayer = []
    caps = []
    lowerLayer = []
    budgetElements = []

    edgeCount = 0

    #create string arrays for nodes
    for i in range(0, int(NUMBER_BUDGETS)):
        budgetElements.append("B" + str((i + 1)))

    for i in range(0, int(NUMBER_UPPER)):
        upperLayer.append("ul" + str((i + 1)))

    for i in range(0, int(NUMBER_CAP)):
        caps.append("c" + str((i + 1)))

    for i in range(0, int(NUMBER_LOWER)):
        lowerLayer.append("ll" + str((i + 1)))

    writeline(f, "#Nodes in the network")
    f.write("set N := S ")
    for budget in budgetElements:
        f.write(budget + " ")
    for upper in upperLayer:
        f.write(upper + " ")
    for cap in caps:
        f.write(cap + " ")
    for lower in lowerLayer:
        f.write(lower + " ")
    writeline(f, "T;")


    writeline(f, "# Network Arcs")
    f.write("set A :=")
    for budget in budgetElements:
        f.write(" (S, " + budget + ")" )
        edgeCount += 1

    # helper var to check that every upper element has an incoming arc
    incomingUpperArc = []
    for budget in budgetElements:
        # make sure every node has at least one outcoming and incoming arc
        randomUpper = randint(0, len(upper) - 1)
        for upper in upperLayer:
            rand = random()
            if (rand < DENSITY_BUDGET or upperLayer.index(upper) == randomUpper):
                f.write(" (" + budget + ", " + upper + ")" )
                edgeCount += 1
                incomingUpperArc.append(upper)

    # check for at least one incoming arc in the cap layer, when not, at a random 
    for upper in upperLayer:
        if (upper not in incomingUpperArc):
            randomBudgetIndex = randint(0, len(budgetElements) - 1)
            f.write(" (" + budgetElements[randomBudgetIndex] + ", " + upper + ")" )
            edgeCount += 1

    # helper var to check that every cap element has an incoming arc
    incomingCapArc = []
    for upper in upperLayer:
        # same here
        randomCap = randint(0, len(caps) - 1)
        for cap in caps:
            rand = random()
            if (rand < DENSITY_UPPER or caps.index(cap) == randomCap):
                f.write(" (" + upper + ", " + cap + ")" )
                edgeCount += 1
                incomingCapArc.append(cap)

    # check for at least one incoming arc in the cap layer, when not, at a random 
    for cap in caps:
        if (cap not in incomingCapArc):
            randomUpperIndex = randint(0, len(upperLayer) - 1)
            f.write(" (" + upperLayer[randomUpperIndex] + ", " + cap + ")" )
            edgeCount += 1

    # helper var to check that every lower element has an incoming arc
    incomingLowerArc = []
    for cap in caps:
        # same here
        randomLower = randint(0, len(lowerLayer) - 1)
        for lower in lowerLayer:
            rand = random()
            if (rand < DENSITY_LOWER or lowerLayer.index(lower) == randomLower):
                f.write(" (" + cap + ", " + lower + ")" )
                edgeCount += 1
                incomingLowerArc.append(lower)

    # check for at least one incoming arc in the lower layer, when not, at a random 
    for lower in lowerLayer:
        if (lower not in incomingLowerArc):
            randomCapIndex = randint(0, len(caps) - 1)
            f.write(" (" + caps[randomCapIndex] + ", " + lower + ")" )
            edgeCount += 1

    # connect all lower elements with the sink
    for lower in lowerLayer:
        f.write(" (" + lower + ", T)")
        edgeCount += 1
    writeline(f, ";")



    writeline(f, "# Budget Arcs")
    f.write("set B :=")
    for budget in budgetElements:
        f.write(" (S, " + budget + ")")
    writeline(f, ";")

    writeline(f, "# Cost (Price) Arcs")
    f.write("set P :=")
    for lower in lowerLayer:
        f.write(" (" + lower + ", T)")
    writeline(f, ";")

    writeline(f, "# Source node")
    writeline(f, "param s := S;")
    writeline(f, "# Sink node")
    writeline(f, "param t := T;")

    writeline(f, "# Capability nodes")
    f.write("set C :=")
    for cap in caps:
        f.write(" " + cap)
    writeline(f, ";")

    writeline(f, "# Application nodes")
    f.write("set LL :=")
    for lower in lowerLayer:
        f.write(" " + lower)
    writeline(f, ";")

    writeline(f, "# Already implemented application nodes")
    f.write("param: z :=")
    for lower in lowerLayer:
        writeline(f, "")
        f.write(lower + " " + str(randint(0,1)))
    writeline(f, ";")

    writeline(f, "# Transition costs")
    f.write("param: tp :=")
    for lower in lowerLayer:
        writeline(f, "")
        f.write(lower + " " + str(randint(TRANSITIONSCOSTS_MIN,TRANSITIONSCOSTS_MAX)))
    writeline(f, ";")

    writeline(f, "# Budget capacity limits")
    f.write("param: b :=")
    for budget in budgetElements:
        writeline(f, "")
        f.write("S " + budget + " " + str(randint(BUDGET_MIN,BUDGET_MAX)))
    writeline(f, ";")

    writeline(f, "# Costs for each application")
    f.write("param: p :=")
    for lower in lowerLayer:
        writeline(f, "")
        f.write(lower + " " + "T " + str(randint(APPLICATIONCOSTS_MIN,APPLICATIONCOSTS_MAX)))
    writeline(f, ";")

    return edgeCount