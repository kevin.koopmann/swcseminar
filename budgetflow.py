from pyomo.environ import *

model = AbstractModel()

# Nodes in the network
model.N = Set()
# Network arcs
model.A = Set(within=model.N*model.N)

# Source node
model.s = Param(within=model.N)
# Sink node
model.t = Param(within=model.N)
# Capability nodes
model.C = Set(within=model.N)
# Application nodes
model.LL = Set(within=model.N)
# Already implemented application nodes
model.z = Param(model.LL, within=NonNegativeIntegers)
# Transition Costs
model.tp = Param(model.LL, within=NonNegativeIntegers)
# Budget Arcs
model.B = Set(within=model.A)
# Cost (Price) Arcs
model.P = Set(within=model.A)
# Budget capacity limits
model.b = Param(model.B, within=NonNegativeIntegers)
# Costs
model.p = Param(model.P, within=NonNegativeIntegers)

# The flow over each arc
model.f = Var(model.A, within=NonNegativeIntegers)
# Transition necessary?
model.tn = Var(model.LL, within=BooleanSet)
# The applications
model.app = Var(model.LL, within=BooleanSet)


## OBJECTIVE
# Minimize the cost flow through the network
def total_rule(model):
    return sum(model.f[i, j] for (i, j) in model.A if j == value(model.t))
model.total = Objective(rule=total_rule, sense=minimize)


## CONSTRAINTS
# Enforce an upper limit on the flow across the budget arcs
def budget_rule(model, i, j):
    return model.f[i, j] <= model.b[i, j]
model.budget = Constraint(model.B, rule=budget_rule)

# Enforce a flow of at least 1 out of each capability
def capability_rule(model, k):
    return sum(model.app[j] for (i,j) in model.A if i == k) >= 1
model.capability = Constraint(model.C, rule=capability_rule)

# Determine whether transition is necessary
def tn1_rule(model, i):
    return model.tn[i] >= model.z[i] - model.app[i]
model.tn1 = Constraint(model.LL, rule=tn1_rule)

def tn2_rule(model, i):
    return model.tn[i] >= model.app[i] - model.z[i]
model.tn2 = Constraint(model.LL, rule=tn2_rule)

# Enforce a flow of either 0 or <cost>+<transition-cost> through the cost arcs
def cost_rule(model, i, j):
    return model.f[i,j] == ((model.app[i] * model.p[i,j]) + (model.tn[i] * model.tp[i])) 
model.cost = Constraint(model.P, rule=cost_rule)

# Enforce flow through each node
def flow_rule(model, k):
    if k == value(model.s) or k == value(model.t):
        return Constraint.Skip
    inFlow = sum(model.f[i, j] for (i, j) in model.A if j == k)
    outFlow = sum(model.f[i, j] for (i, j) in model.A if i == k)
    return inFlow == outFlow
model.flow = Constraint(model.N, rule=flow_rule)
