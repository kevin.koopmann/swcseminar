# Optimizing Enterprise Architectures Considering Different Budgets
_Work in progress_

## Prerequisites
 * Python 2.7/3.5 or higher
 * Pyomo
 * Solver capable of handling quadratic constraints (e.g. Gurobi)
 * Graphviz
 * pyGraphviz

## How to run
Create an `output` directory. Then simply run `solve.py run budgetflow.dat`.

## How to create example data using generator.py
Use the `-g` flag to generate a random architecture. This overwrites the specified `INPUT` file. See `solve.py -h` for information on the required arguments.

```bash
python solve.py run budgetflow.dat -g 30 30 50 40 100 1000 1000000 10000000 100 2500 0.1 0.1 0.1
```

## Use automated runtime testing
Use the `autotest` subcommand to perform an automated evaluation of solver runtime using the automated testset generator with specified parameters. For example, to determine the average runtime of the example above over 20 runs, execute the following command. 

```bash
python solve.py autotest 1 30 30 50 40 100 1000 1000000 10000000 100 2500 0.1 0.1 0.1 output.csv --average 20
```

To perform 5 test runs with averaging over 10 iterations, as well as automatic incrementation of node counts and decrementation of connection densities, try the following.

```bash
python solve.py autotest 5 10 20 60 40 100 1000 1000000 10000000 100 2500 0.5 0.5 0.5 out.csv --average 10 --increment 0 10 --increment 1 20 --increment 2 60 --increment 3 40 --decrement 10 0.1 --decrement 11 0.1 --decrement 12 0.1
```

Refer to the CLI help using `solve.py autotest -h` for additional information.